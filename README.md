# canta-theme-git

Flat Material Design theme for GTK 3, GTK 2 and Gnome-Shell

Contains **canta-gtk-theme-git** and **canta-icon-theme-git**

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/canta-theme-git.git
```

https://github.com/vinceliuice/Canta-theme

